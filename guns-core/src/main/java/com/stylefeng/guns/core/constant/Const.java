package com.stylefeng.guns.core.constant;

/**
 * 系统常量
 *
 * @author fengshuonan
 * @date 2017年2月12日 下午9:42:53
 */
public interface Const {

    /**
     * 机构root id
     */
    Long NODE_ROOT_ID = 0L;

    /**
     * 机构 root 名称
     */
    String NODE_ROOT_NAME = "--";

}
