@/*
    图片:
    name : 名称
    id : 图片id
@*/
<div class="form-group">
    <label class="col-sm-1 control-label head-scu-label">${name}</label>
    <div class="col-sm-${col}">
        <div id="${id}PreId">
            <div class="web-image"><img width="${width!100}px" height="${height!100}px" class="img-rounded"
                @if(isEmpty(value)){
                    src="#"></div>
                @}else{
                    src="${value}"></div>
                @}
        </div>
    </div>
    <div class="col-sm-1">
        <div class="head-scu-btn upload-btn" id="${id}BtnId">
            <i class="fa fa-upload"></i>&nbsp;上传
        </div>
    </div>
    <input type="hidden" id="${id}" value="${value!}"/>
</div>
@if(isNotEmpty(underline) && underline == 'true'){
    <div class="hr-line-dashed"></div>
@}


