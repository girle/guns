/**
 * 友情链接管理初始化
 */
var Links = {
    id: "LinksTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Links.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '图片', field: 'logo', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row){
                    if(row.link){
                        return '<a href="'+row.link+'" target="_blank"><img width="100px" height="50px" src="'+value+'" class="img-rounded" ></a>';
                    }
                    return '<img width="100px" height="50px" src="'+value+'" class="img-rounded" >';
                }
            },
            {title: '名称', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'statusTag', visible: true, align: 'center', valign: 'middle'},
            {title: '排序号', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Links.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Links.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加友情链接
 */
Links.openAddLinks = function () {
    var index = layer.open({
        type: 2,
        title: '添加友情链接',
        area: ['900px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/links/links_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看友情链接详情
 */
Links.openLinksDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '友情链接详情',
            area: ['900px', '450px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/links/links_update/' + Links.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除友情链接
 */
Links.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/links/delete", function (data) {
            Feng.success("删除成功!");
            Links.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("linksId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询友情链接列表
 */
Links.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Links.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Links.initColumn();
    var table = new BSTable(Links.id, "/links/list", defaultColunms);
    table.setPaginationType("client");
    Links.table = table.init();
});
