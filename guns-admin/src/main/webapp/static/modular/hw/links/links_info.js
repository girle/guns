/**
 * 初始化友情链接详情对话框
 */
var LinksInfoDlg = {
    linksInfoData : {}
};

/**
 * 清除数据
 */
LinksInfoDlg.clearData = function() {
    this.linksInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LinksInfoDlg.set = function(key, val) {
    this.linksInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LinksInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LinksInfoDlg.close = function() {
    parent.layer.close(window.parent.Links.layerIndex);
}

/**
 * 收集数据
 */
LinksInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('logo')
    .set('link')
    .set('status')
    .set('num')
}

/**
 * 提交添加
 */
LinksInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/links/add", function(data){
        Feng.success("添加成功!");
        window.parent.Links.table.refresh();
        LinksInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.linksInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LinksInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/links/update", function(data){
        Feng.success("修改成功!");
        window.parent.Links.table.refresh();
        LinksInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.linksInfoData);
    ajax.start();
}

$(function() {
    // 初始化状态
    $("#status").val($("#statusValue").val());
    // 初始化图片上传
    var avatarUp = new $WebUpload("logo");
    avatarUp.init();
});
