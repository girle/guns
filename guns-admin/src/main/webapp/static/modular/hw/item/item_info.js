/**
 * 初始化栏目详情对话框
 */
var ItemInfoDlg = {
    itemInfoData : {}
};

/**
 * 清除数据
 */
ItemInfoDlg.clearData = function() {
    this.itemInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemInfoDlg.set = function(key, val) {
    this.itemInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ItemInfoDlg.close = function() {
    parent.layer.close(window.parent.Item.layerIndex);
}


/**
 * 点击栏目ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 */
ItemInfoDlg.onClickItem = function(e, treeId, treeNode) {
    $("#pName").attr("value", ItemInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
}

/**
 * 显示栏目选择的树
 *
 * @returns
 */
ItemInfoDlg.showItemSelectTree = function() {
    var pName = $("#pName");
    var pNameOffset = $("#pName").offset();
    $("#parentItemMenu").css({
        left : pNameOffset.left + "px",
        top : pNameOffset.top + pName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

/**
 * 隐藏栏目选择的树
 */
ItemInfoDlg.hideItemSelectTree = function() {
    $("#parentItemMenu").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentItemMenu" || $(
        event.target).parents("#parentItemMenu").length > 0)) {
        ItemInfoDlg.hideItemSelectTree();
    }
}

/**
 * 收集数据
 */
ItemInfoDlg.collectData = function() {
    this
    .set('id')
    .set('pid')
    .set('code')
    .set('name')
    .set('link')
    .set('num')
    .set('status');
}

/**
 * 提交添加
 */
ItemInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/item/add", function(data){
        Feng.success("添加成功!");
        window.parent.Item.table.refresh();
        ItemInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ItemInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/item/update", function(data){
        Feng.success("修改成功!");
        window.parent.Item.table.refresh();
        ItemInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemInfoData);
    ajax.start();
}

$(function() {
    //初始化状态选项
    $("#status").val($("#statusValue").val());

    //初始化栏目树
    var ztree = new $ZTree("parentItemTree", "/item/tree");
    ztree.bindOnClick(ItemInfoDlg.onClickItem);
    ztree.init();
    ItemInfoDlg.zTreeInstance = ztree;
});
