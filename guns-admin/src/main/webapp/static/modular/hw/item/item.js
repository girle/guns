/**
 * 栏目管理初始化
 */
var Item = {
    id: "ItemTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Item.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '编码', field: 'code', visible: true, align: 'center', valign: 'middle'},
        {title: '链接', field: 'link', visible: true, align: 'center', valign: 'middle'},
        {title: '排序号', field: 'num', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'statusTag', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Item.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Item.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加栏目
 */
Item.openAddItem = function () {
    var index = layer.open({
        type: 2,
        title: '添加栏目',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/item/item_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看栏目详情
 */
Item.openItemDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '栏目详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/item/item_update/' + Item.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除栏目
 */
Item.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/item/delete", function (data) {
            Feng.success("删除成功!");
            Item.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("itemId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询栏目列表
 */
Item.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Item.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Item.initColumn();
/*    var table = new BSTable(Item.id, "/item/list", defaultColunms);
    table.setPaginationType("client");
    Item.table = table.init();*/
    var table = new BSTreeTable(Item.id, "/item/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    Item.table = table;
});
