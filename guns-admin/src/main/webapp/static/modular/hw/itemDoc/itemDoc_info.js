/**
 * 初始化文章详情对话框
 */
var ItemDocInfoDlg = {
    itemDocInfoData : {}
};


/**
 * 点击栏目input框时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 */
ItemDocInfoDlg.onClickItem = function (e, treeId, treeNode) {
    $("#itemName").attr("value", instance.getSelectedVal());
    $("#itemid").attr("value", treeNode.id);
};

/**
 * 显示栏目选择的树
 *
 * @returns
 */
ItemDocInfoDlg.showItemSelectTree = function () {
    var itemName = $("#itemName");
    var itemNameOffset = $("#itemName").offset();
    $("#menuContent").css({
        left: itemNameOffset.left + "px",
        top: itemNameOffset.top + itemName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
};

/**
 * 显示用户详情栏目选择的树
 *
 * @returns
 */
ItemDocInfoDlg.showInfoItemSelectTree = function () {
    var cityObj = $("#itemName");
    var cityPosition = $("#itemName").position();
    $("#menuContent").css({
        left: cityPosition.left + "px",
        top: cityPosition.top + cityObj.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
};

/**
 * 隐藏栏目选择的树
 */
ItemDocInfoDlg.hideItemSelectTree = function () {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
};

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(
        event.target).parents("#menuContent").length > 0)) {
        ItemDocInfoDlg.hideItemSelectTree();
    }
}

/**
 * 清除数据
 */
ItemDocInfoDlg.clearData = function() {
    this.itemDocInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemDocInfoDlg.set = function(key, val) {
    this.itemDocInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemDocInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ItemDocInfoDlg.close = function() {
    parent.layer.close(window.parent.ItemDoc.layerIndex);
}

/**
 * 收集数据
 */
ItemDocInfoDlg.collectData = function() {
    this.itemDocInfoData['content'] = ItemDocInfoDlg.editor.txt.html();
    this.set('id').set('itemid').set('title').set('photo').set('num').set('status').set('pv').set('uv').set('creater')
}

/**
 * 提交添加
 */
ItemDocInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemDoc/add", function(data){
        Feng.success("添加成功!");
        window.parent.ItemDoc.table.refresh();
        ItemDocInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemDocInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ItemDocInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemDoc/update", function(data){
        Feng.success("修改成功!");
        window.parent.ItemDoc.table.refresh();
        ItemDocInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemDocInfoData);
    ajax.start();
}

$(function() {
    // 初始化状态
    $("#status").val($("#statusValue").val());
    //  初始化栏目
    var ztree = new $ZTree("treeDemo", "/item/tree");
    ztree.bindOnClick(ItemDocInfoDlg.onClickItem);
    ztree.init();
    instance = ztree;
    // 初始化图片上传
    var avatarUp = new $WebUpload("photo");
    avatarUp.init();
    //初始化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    editor.customConfig.uploadImgParams = {};
    editor.customConfig.uploadFileName = "file";
    editor.customConfig.uploadImgServer = "/fileUpload";
    editor.customConfig.uploadImgHooks = {
        customInsert: function (insertImg, result, editor) {
            // result 必须是一个 JSON 格式字符串！！！否则报错
            // 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
            // insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
            // 举例：假如上传图片成功后，服务器端返回的是 {url:'....'} 这种格式，即可这样插入图片：
            if(result.code==200){
                insertImg(result.data.relativeUrl)
            }
        }
    };
    editor.create();
    editor.txt.html($("#contentVal").val());
    ItemDocInfoDlg.editor = editor;

});
