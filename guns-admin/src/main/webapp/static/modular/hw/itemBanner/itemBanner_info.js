/**
 * 初始化栏目-Banner详情对话框
 */
var ItemBannerInfoDlg = {
    itemBannerInfoData : {}
};


/**
 * 点击栏目input框时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 */
ItemBannerInfoDlg.onClickItem = function (e, treeId, treeNode) {
    $("#itemName").attr("value", instance.getSelectedVal());
    $("#itemid").attr("value", treeNode.id);
};

/**
 * 显示栏目选择的树
 *
 * @returns
 */
ItemBannerInfoDlg.showItemSelectTree = function () {
    var itemName = $("#itemName");
    var itemNameOffset = $("#itemName").offset();
    $("#menuContent").css({
        left: itemNameOffset.left + "px",
        top: itemNameOffset.top + itemName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
};

/**
 * 显示用户详情栏目选择的树
 *
 * @returns
 */
ItemBannerInfoDlg.showInfoItemSelectTree = function () {
    var cityObj = $("#itemName");
    var cityPosition = $("#itemName").position();
    $("#menuContent").css({
        left: cityPosition.left + "px",
        top: cityPosition.top + cityObj.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
};

/**
 * 隐藏栏目选择的树
 */
ItemBannerInfoDlg.hideItemSelectTree = function () {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
};

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(
        event.target).parents("#menuContent").length > 0)) {
        ItemBannerInfoDlg.hideItemSelectTree();
    }
}
/**
 * 清除数据
 */
ItemBannerInfoDlg.clearData = function() {
    this.itemBannerInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemBannerInfoDlg.set = function(key, val) {
    this.itemBannerInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemBannerInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ItemBannerInfoDlg.close = function() {
    parent.layer.close(window.parent.ItemBanner.layerIndex);
}

/**
 * 收集数据
 */
ItemBannerInfoDlg.collectData = function() {
    this
    .set('id')
    .set('itemid')
    .set('title')
    .set('logo')
    .set('link')
    .set('num')
    .set('status')
    .set('pv')
    .set('uv');
}

/**
 * 提交添加
 */
ItemBannerInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemBanner/add", function(data){
        Feng.success("添加成功!");
        window.parent.ItemBanner.table.refresh();
        ItemBannerInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemBannerInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ItemBannerInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemBanner/update", function(data){
        Feng.success("修改成功!");
        window.parent.ItemBanner.table.refresh();
        ItemBannerInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemBannerInfoData);
    ajax.start();
}

$(function() {
    //初始化状态选项
    $("#status").val($("#statusValue").val());

    var ztree = new $ZTree("treeDemo", "/item/tree");
    ztree.bindOnClick(ItemBannerInfoDlg.onClickItem);
    ztree.init();
    instance = ztree;
    // 初始化图片上传
    var avatarUp = new $WebUpload("logo");
    avatarUp.init();

});
