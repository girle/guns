/**
 * 栏目-Banner管理初始化
 */
var ItemBanner = {
    id: "ItemBannerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ItemBanner.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '栏目ID', field: 'itemName', visible: true, align: 'center', valign: 'middle'},
            {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '图标', field: 'logo', visible: true, align: 'center', valign: 'middle',width:'200px',
                formatter: function(value,row){
                    if(row.link){
                        return '<a href="'+row.link+'" target="_blank"><img width="100%" height="50px" src="'+value+'" class="img-rounded" ></a>';
                    }
                    return '<img width="100%" height="50px" src="'+value+'" class="img-rounded" >';
                }
            },
            {title: '排序号', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'statusTag', visible: true, align: 'center', valign: 'middle'},
            {title: '点击量', field: 'pv', visible: true, align: 'center', valign: 'middle'},
            {title: '访问量', field: 'uv', visible: true, align: 'center', valign: 'middle'},
            {title: '作者', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ItemBanner.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ItemBanner.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加栏目-Banner
 */
ItemBanner.openAddItemBanner = function () {
    var index = layer.open({
        type: 2,
        title: '添加栏目-Banner',
        area: ['900px', '650px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/itemBanner/itemBanner_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看栏目-Banner详情
 */
ItemBanner.openItemBannerDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '栏目-Banner详情',
            area: ['900px', '650px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/itemBanner/itemBanner_update/' + ItemBanner.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除栏目-Banner
 */
ItemBanner.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/itemBanner/delete", function (data) {
            Feng.success("删除成功!");
            ItemBanner.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("itemBannerId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询栏目-Banner列表
 */
ItemBanner.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ItemBanner.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ItemBanner.initColumn();
    var table = new BSTable(ItemBanner.id, "/itemBanner/list", defaultColunms);
    table.setPaginationType("client");
    ItemBanner.table = table.init();
});
