package com.stylefeng.guns.core.common.constant.cache;

/**
 * 缓存标识前缀集合,常用在ConstantFactory类中
 *
 * @author fengshuonan
 * @date 2017-04-25 9:37
 */
public interface CacheKey {

    /**
     * 角色名称(多个)
     */
    String ROLES_NAME = "roles_name_";

    /**
     * 角色名称(单个)
     */
    String SINGLE_ROLE_NAME = "single_role_name_";

    /**
     * 角色英文名称
     */
    String SINGLE_ROLE_TIP = "single_role_tip_";

    /**
     * 用户名称
     */
    String USER_NAME = "user_name_";

    /**
     * 用户账号
     */
    String USER_ACCOUNT = "user_account_";

    /**
     * 部门名称
     */
    String DEPT_NAME = "dept_name_";

    /**
     * 部门名称
     */
    String ITEM_NAME = "item_name_";

}
