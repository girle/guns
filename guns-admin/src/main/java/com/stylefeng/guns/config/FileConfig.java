package com.stylefeng.guns.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "file-config")
@Component
@Data
public class FileConfig {

    //磁盘目录
    private String fileDiskPath = "d:/tmp";
    //访问目录
    private String fileVirPath;
    //DNS
    private String fileDns;

}
