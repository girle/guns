package com.stylefeng.guns.modular.hw.dao;

import com.stylefeng.guns.modular.hw.model.ItemBanner;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 栏目-banner Mapper 接口
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
public interface ItemBannerMapper extends BaseMapper<ItemBanner> {

    List<ItemBanner> findList(@Param("itemCode") String itemCode);

}
