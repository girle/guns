package com.stylefeng.guns.modular.hw.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 栏目-banner
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
@Data
@TableName("hw_item_banner")
public class ItemBanner extends Model<ItemBanner> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 栏目ID
     */
    private Long itemid;
    /**
     * 标题
     */
    private String title;
    /**
     * 图标
     */
    private String logo;
    /**
     * 链接
     */
    private String link;
    /**
     * 排序号
     */
    private Integer num;
    /**
     * 状态 :  0:禁用 1:启用
     */
    private Integer status;
    /**
     * 点击量
     */
    private Integer pv;
    /**
     * 访问量
     */
    private Integer uv;
    /**
     * 作者
     */
    private Long creater;
    /**
     * 创建时间
     */
    private Date createtime;
    /**
     * 更新时间
     */
    private Date updatetime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItemBanner{" +
        "id=" + id +
        ", itemid=" + itemid +
        ", title=" + title +
        ", logo=" + logo +
        ", link=" + link +
        ", num=" + num +
        ", status=" + status +
        ", pv=" + pv +
        ", uv=" + uv +
        ", creater=" + creater +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
