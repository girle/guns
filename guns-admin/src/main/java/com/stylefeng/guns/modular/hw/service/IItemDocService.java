package com.stylefeng.guns.modular.hw.service;

import com.stylefeng.guns.modular.hw.model.ItemDoc;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章 服务类
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
public interface IItemDocService extends IService<ItemDoc> {
    /**
     * # -文章查询- #
     * @auther: wangchuan  2018/9/10
     */
    List<Map<String, Object>> selectDocs(String title,Long itemid);

    /**
     * # -根据栏目code查找文章- #
     * @auther: wangchuan  2018/9/12
     */
    List<ItemDoc> findDocsByItemCode(String itemCode,Integer size);


}
