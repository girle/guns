package com.stylefeng.guns.modular.hw.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.hw.model.Item;
import com.stylefeng.guns.modular.hw.service.IItemService;
import com.stylefeng.guns.modular.hw.warper.ItemWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 栏目控制器
 *
 * @author fengshuonan
 * @Date 2018-09-03 21:32:38
 */
@Controller
@RequestMapping("/item")
public class ItemController extends BaseController {

    private String PREFIX = "/hw/item/";

    @Autowired
    private IItemService itemService;

    /**
     * 跳转到栏目首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "item.html";
    }

    /**
     * 跳转到添加栏目
     */
    @RequestMapping("/item_add")
    public String itemAdd() {
        return PREFIX + "item_add.html";
    }

    /**
     * 跳转到修改栏目
     */
    @RequestMapping("/item_update/{itemId}")
    public String itemUpdate(@PathVariable Long itemId, Model model) {
        Item item = itemService.selectById(itemId);
        model.addAttribute("item",item);
        model.addAttribute("pName", ConstantFactory.me().getItemName(item.getPid()));
        LogObjectHolder.me().set(item);
        return PREFIX + "item_edit.html";
    }

    /**
     * 获取栏目列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper wrapper = new EntityWrapper();
        if(StrUtil.isNotBlank(condition))
            wrapper.like("name",condition);
        List<Map<String,Object>> list =  itemService.selectMaps(wrapper);
        return super.warpObject(new ItemWarpper(list));
    }

    /**
     * 获取部门的tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = itemService.tree();
        tree.add(ZTreeNode.createParent());
        return tree;
    }

    /**
     * 新增栏目
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Item item) {
        if (ToolUtil.isOneEmpty(item, item.getName(), item.getCode())) {
            throw new GunsException(BizExceptionEnum.REQUEST_NULL);
        }
        itemSetPids(item);
        itemService.insert(item);
        return SUCCESS_TIP;
    }

    /**
     * 删除栏目
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer itemId) {
        itemService.deleteById(itemId);
        return SUCCESS_TIP;
    }

    /**
     * 修改栏目
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Item item) {
        itemService.updateById(item);
        return SUCCESS_TIP;
    }

    /**
     * 栏目详情
     */
    @RequestMapping(value = "/detail/{itemId}")
    @ResponseBody
    public Object detail(@PathVariable("itemId") Integer itemId) {
        return itemService.selectById(itemId);
    }

    private void itemSetPids(Item item) {
        if (ToolUtil.isEmpty(item.getPid()) || item.getPid().equals(0L)) {
            item.setPid(0L);
            item.setPids("[0],");
        } else {
            Long pid = item.getPid();
            Item temp = itemService.selectById(pid);
            String pids = temp.getPids();
            item.setPid(pid);
            item.setPids(pids + "[" + pid + "],");
        }
    }

}
