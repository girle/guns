package com.stylefeng.guns.modular.hw.service.impl;

import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.hw.model.Item;
import com.stylefeng.guns.modular.hw.dao.ItemMapper;
import com.stylefeng.guns.modular.hw.service.IItemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 栏目 服务实现类
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
@Service
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements IItemService {

    @Override
    public List<ZTreeNode> tree() {
        return this.baseMapper.tree();
    }
}
