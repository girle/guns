package com.stylefeng.guns.modular.hw.service;

import com.stylefeng.guns.modular.hw.model.Links;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 友情链接 服务类
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
public interface ILinksService extends IService<Links> {

}
