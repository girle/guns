package com.stylefeng.guns.modular.main;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.hw.dao.ItemBannerMapper;
import com.stylefeng.guns.modular.hw.model.ItemBanner;
import com.stylefeng.guns.modular.hw.model.ItemDoc;
import com.stylefeng.guns.modular.hw.model.Links;
import com.stylefeng.guns.modular.hw.service.IItemBannerService;
import com.stylefeng.guns.modular.hw.service.IItemDocService;
import com.stylefeng.guns.modular.hw.service.ILinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 登录控制器
 *
 * @author fengshuonan
 * @Date 2017年1月10日 下午8:25:24
 */
@Controller
@RequestMapping("/main")
public class IndexController extends BaseController {

    private String PREFIX = "/main/";

    @Autowired
    private IItemBannerService itemBannerService;

    @Autowired
    private IItemDocService itemDocService;

    @Autowired
    private ILinksService linksService;

    /**
     * 跳转到主页
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(Model model) {
        return PREFIX+"index.html";
    }

    //首页
    @RequestMapping(value = "/sy", method = RequestMethod.GET)
    public String sy(Model model) {
        //首页banner图片
        List<ItemBanner> banners =  itemBannerService.findList("SY");
        model.addAttribute("banners",banners);
        //热门项目推荐 - RMXMTJ
        List<ItemDoc> RMXMTJ = itemDocService.findDocsByItemCode("RMXMTJ",8);
        model.addAttribute("RMXMTJ",RMXMTJ);
        //拓展新闻 - TZXW
        List<ItemDoc> TZXW = itemDocService.findDocsByItemCode("TZXW",4);
        model.addAttribute("TZXW",TZXW);
        //案例分享 - ALFX
        List<ItemDoc> ALFX = itemDocService.findDocsByItemCode("ALFX",6);
        model.addAttribute("ALFX",ALFX);
        //合作伙伴
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("status",1);
        wrapper.orderBy("num");
        List<Links> links = linksService.selectList(wrapper);
        model.addAttribute("links",links);
        return PREFIX+"sy.html";
    }

    //拓展项目
    @RequestMapping(value = "/tzxm", method = RequestMethod.GET)
    public String tzxm(Model model) {

        return PREFIX+"tzxm.html";
    }


    //联系我们
    @RequestMapping(value = "/lxwm", method = RequestMethod.GET)
    public String lxwm(Model model) {
        //banner图片
        List<ItemBanner> banners =  itemBannerService.findList("LXWM");
        model.addAttribute("banners",banners);
        //联系我们
        List<ItemDoc> lxwm = itemDocService.findDocsByItemCode("LXWM",1);
        if(lxwm.size()>0){
            model.addAttribute("lxwm",lxwm.get(0));
        }
        return PREFIX+"lxwm.html";
    }


}
