package com.stylefeng.guns.modular.hw.warper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;

import java.util.Map;

/**
 * 栏目列表的包装
 *
 * @author fengshuonan
 * @date 2017年4月25日 18:10:31
 */
public class ItemWarpper extends BaseControllerWarpper {

    public ItemWarpper(Object list) {
        super(list);
    }

    @Override
    public void warpTheMap(Map<String, Object> map) {
        map.put("statusTag",ConstantFactory.me().getStatusName((Integer) map.get("status")));
    }

}
