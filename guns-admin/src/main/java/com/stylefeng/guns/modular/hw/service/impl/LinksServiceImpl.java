package com.stylefeng.guns.modular.hw.service.impl;

import com.stylefeng.guns.modular.hw.model.Links;
import com.stylefeng.guns.modular.hw.dao.LinksMapper;
import com.stylefeng.guns.modular.hw.service.ILinksService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 友情链接 服务实现类
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
@Service
public class LinksServiceImpl extends ServiceImpl<LinksMapper, Links> implements ILinksService {

}
