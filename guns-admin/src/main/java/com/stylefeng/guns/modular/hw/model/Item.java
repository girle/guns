package com.stylefeng.guns.modular.hw.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 栏目
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
@Data
@TableName("hw_item")
public class Item extends Model<Item> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 编码
     */
    private String code;
    /**
     * 父id
     */
    private Long pid;
    /**
     * 父级ids
     */
    private String pids;
    /**
     * 名称
     */
    private String name;
    /**
     * 链接
     */
    private String link;
    /**
     * 排序号
     */
    private Integer num;
    /**
     * 状态 :  0:禁用 1:启用
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createtime;
    /**
     * 更新时间
     */
    private Date updatetime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Item{" +
        "id=" + id +
        ", code=" + code +
        ", pid=" + pid +
        ", pids=" + pids +
        ", name=" + name +
        ", link=" + link +
        ", num=" + num +
        ", status=" + status +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
