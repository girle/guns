package com.stylefeng.guns.modular.hw.service.impl;

import com.stylefeng.guns.modular.hw.model.ItemDoc;
import com.stylefeng.guns.modular.hw.dao.ItemDocMapper;
import com.stylefeng.guns.modular.hw.service.IItemDocService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章 服务实现类
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
@Service
public class ItemDocServiceImpl extends ServiceImpl<ItemDocMapper, ItemDoc> implements IItemDocService {
    @Override
    public List<Map<String, Object>> selectDocs(String title, Long itemid) {
        return baseMapper.selectDocs(title,itemid);
    }


    @Override
    public List<ItemDoc> findDocsByItemCode(String itemCode, Integer size) {
        return baseMapper.findDocsByItemCode(itemCode,size);
    }
}
