package com.stylefeng.guns.modular.hw.service;

import com.stylefeng.guns.modular.hw.model.ItemBanner;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 栏目-banner 服务类
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
public interface IItemBannerService extends IService<ItemBanner> {

    List<ItemBanner> findList(String itemCode);

}
