package com.stylefeng.guns.modular.hw.service.impl;

import com.stylefeng.guns.modular.hw.model.ItemBanner;
import com.stylefeng.guns.modular.hw.dao.ItemBannerMapper;
import com.stylefeng.guns.modular.hw.service.IItemBannerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 栏目-banner 服务实现类
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
@Service
public class ItemBannerServiceImpl extends ServiceImpl<ItemBannerMapper, ItemBanner> implements IItemBannerService {

    @Override
    public List<ItemBanner> findList(String itemCode) {
        return baseMapper.findList(itemCode);
    }
}
