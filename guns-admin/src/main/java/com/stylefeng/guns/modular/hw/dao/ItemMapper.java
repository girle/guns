package com.stylefeng.guns.modular.hw.dao;

import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.hw.model.Item;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 栏目 Mapper 接口
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
public interface ItemMapper extends BaseMapper<Item> {

    List<ZTreeNode> tree();
}
