package com.stylefeng.guns.modular.hw.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.modular.hw.model.Item;
import com.stylefeng.guns.modular.hw.model.ItemBanner;
import com.stylefeng.guns.modular.hw.service.IItemBannerService;
import com.stylefeng.guns.modular.hw.service.IItemService;
import com.stylefeng.guns.modular.hw.warper.ItemBannerWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 栏目-Banner控制器
 *
 * @author fengshuonan
 * @Date 2018-09-03 21:33:12
 */
@Controller
@RequestMapping("/itemBanner")
public class ItemBannerController extends BaseController {

    private String PREFIX = "/hw/itemBanner/";

    @Autowired
    private IItemBannerService itemBannerService;

    @Autowired
    private IItemService itemService;

    /**
     * 跳转到栏目-Banner首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "itemBanner.html";
    }

    /**
     * 跳转到添加栏目-Banner
     */
    @RequestMapping("/itemBanner_add")
    public String itemBannerAdd(Model model) {
        //model.addAttribute("itemTag",itemTag(null));
        return PREFIX + "itemBanner_add.html";
    }

    /**
     * 跳转到修改栏目-Banner
     */
    @RequestMapping("/itemBanner_update/{itemBannerId}")
    public String itemBannerUpdate(@PathVariable Long itemBannerId, Model model) {
        ItemBanner itemBanner = itemBannerService.selectById(itemBannerId);
        model.addAttribute("item",itemBanner);
        model.addAttribute("itemName",ConstantFactory.me().getItemName(itemBanner.getItemid()));
        LogObjectHolder.me().set(itemBanner);
        return PREFIX + "itemBanner_edit.html";
    }

    private String itemTag(Long itemId){
        StringBuilder sb = new StringBuilder();
        List<Item> itemList = itemService.selectList(null);
        for(Item item : itemList){
            if(item.getId().equals(itemId))
                sb.append("<option value='"+item.getId()+"'selected>"+item.getName()+"</option>");
            else
                sb.append("<option value='"+item.getId()+"'>"+item.getName()+"</option>");
        }
        return sb.toString();
    }

    /**
     * 获取栏目-Banner列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper wrapper = new EntityWrapper();
        if(StrUtil.isNotBlank(condition))
            wrapper.like("title",condition);
        List<Map<String,Object>> list =  itemBannerService.selectMaps(wrapper);
        return super.warpObject(new ItemBannerWarpper(list));
    }

    /**
     * 新增栏目-Banner
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ItemBanner itemBanner) {
        itemBanner.setCreater(ShiroKit.getUser().getId());
        itemBannerService.insert(itemBanner);
        return SUCCESS_TIP;
    }

    /**
     * 删除栏目-Banner
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long itemBannerId) {
        itemBannerService.deleteById(itemBannerId);
        return SUCCESS_TIP;
    }

    /**
     * 修改栏目-Banner
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ItemBanner itemBanner) {
        itemBanner.setCreater(ShiroKit.getUser().getId());
        itemBannerService.updateById(itemBanner);
        return SUCCESS_TIP;
    }

    /**
     * 栏目-Banner详情
     */
    @RequestMapping(value = "/detail/{itemBannerId}")
    @ResponseBody
    public Object detail(@PathVariable("itemBannerId") Integer itemBannerId) {
        return itemBannerService.selectById(itemBannerId);
    }
}
