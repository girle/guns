package com.stylefeng.guns.modular.hw.service;

import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.hw.model.Item;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 栏目 服务类
 * </p>
 *
 * @author 胡一刀
 * @since 2018-09-03
 */
public interface IItemService extends IService<Item> {

    List<ZTreeNode> tree();
}
