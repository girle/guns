package com.stylefeng.guns.modular.hw.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.hw.model.ItemDoc;
import com.stylefeng.guns.modular.hw.service.IItemDocService;
import com.stylefeng.guns.modular.hw.warper.ItemDocWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 文章 控制器
 *
 * @author wangchuan
 * @Date 2018-09-10 11:31:20
 */
@Controller
@RequestMapping("/itemDoc")
public class ItemDocController extends BaseController {

    private String PREFIX = "/hw/itemDoc/";

    @Autowired
    private IItemDocService itemDocService;

    /**
     * 跳转到户外首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "itemDoc.html";
    }

    /**
     * 跳转到添加户外
     */
    @RequestMapping("/itemDoc_add/{itemId}")
    public String itemDocAdd(@PathVariable Long itemId, Model model) {
        model.addAttribute("itemid",itemId);
        model.addAttribute("itemName",ConstantFactory.me().getItemName(itemId));
        return PREFIX + "itemDoc_add.html";
    }

    /**
     * 跳转到修改户外
     */
    @RequestMapping("/itemDoc_update/{itemDocId}")
    public String itemDocUpdate(@PathVariable Long itemDocId, Model model) {
        ItemDoc itemDoc = itemDocService.selectById(itemDocId);
        model.addAttribute("item",itemDoc);
        model.addAttribute("itemName",ConstantFactory.me().getItemName(itemDoc.getItemid()));
        LogObjectHolder.me().set(itemDoc);
        return PREFIX + "itemDoc_edit.html";
    }

    /**
     * 获取户外列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(required = false) String title,@RequestParam(required = false) Long itemid) {
        List<Map<String,Object>> list = itemDocService.selectDocs(title,itemid);
        return new ItemDocWarpper(list).warp();
    }

    /**
     * 新增户外
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ItemDoc itemDoc) {
        itemDocService.insert(itemDoc);
        return SUCCESS_TIP;
    }

    /**
     * 删除户外
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long itemDocId) {
        itemDocService.deleteById(itemDocId);
        return SUCCESS_TIP;
    }

    /**
     * 修改户外
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ItemDoc itemDoc) {
        if (ToolUtil.isOneEmpty(itemDoc, itemDoc.getId(), itemDoc.getTitle(), itemDoc.getContent())) {
            throw new GunsException(BizExceptionEnum.REQUEST_NULL);
        }
        itemDocService.updateById(itemDoc);
        return SUCCESS_TIP;
    }

    /**
     * 户外详情
     */
    @RequestMapping(value = "/detail/{itemDocId}")
    @ResponseBody
    public Object detail(@PathVariable("itemDocId") Long itemDocId) {
        return itemDocService.selectById(itemDocId);
    }
}
