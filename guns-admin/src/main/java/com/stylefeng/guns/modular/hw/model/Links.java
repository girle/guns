package com.stylefeng.guns.modular.hw.model;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 友情链接
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
@Data
@TableName("hw_links")
public class Links extends Model<Links> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 图片
     */
    private String logo;
    /**
     * 链接
     */
    private String link;
    /**
     * 状态 :  0:禁用 1:启用
     */
    private Integer status;
    /**
     * 排序号
     */
    private Integer num;
    /**
     * 创建时间
     */
    private Date createtime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Links{" +
        "id=" + id +
        ", title=" + title +
        ", logo=" + logo +
        ", link=" + link +
        ", status=" + status +
        ", num=" + num +
        ", createtime=" + createtime +
        "}";
    }
}
