package com.stylefeng.guns.modular.hw.dao;

import com.stylefeng.guns.modular.hw.model.ItemDoc;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章 Mapper 接口
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
public interface ItemDocMapper extends BaseMapper<ItemDoc> {

    List<Map<String, Object>> selectDocs(@Param("title") String title, @Param("itemid") Long itemid);

    List<ItemDoc> findDocsByItemCode(@Param("itemCode") String itemCode, @Param("size") Integer size);
}
