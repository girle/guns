package com.stylefeng.guns.modular.hw.warper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;

import java.util.Map;

/**
 * 文章列表的包装
 *
 * @author fengshuonan
 * @date 2017年4月25日 18:10:31
 */
public class ItemDocWarpper extends BaseControllerWarpper {

    public ItemDocWarpper(Object list) {
        super(list);
    }

    @Override
    public void warpTheMap(Map<String, Object> map) {
        map.put("statusTag",ConstantFactory.me().getStatusName((Integer) map.get("status")));
        map.put("itemName",ConstantFactory.me().getItemName((Long) map.get("itemid")));
        map.put("userName",ConstantFactory.me().getUserNameById((Long) map.get("creater")));
    }

}
