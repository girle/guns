package com.stylefeng.guns.modular.hw.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.hw.warper.ItemBannerWarpper;
import com.stylefeng.guns.modular.hw.warper.LinksWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.hw.model.Links;
import com.stylefeng.guns.modular.hw.service.ILinksService;

import java.util.List;
import java.util.Map;

/**
 * 友情链接 控制器
 *
 * @author wangchuan
 * @Date 2018-09-10 11:32:39
 */
@Controller
@RequestMapping("/links")
public class LinksController extends BaseController {

    private String PREFIX = "/hw/links/";

    @Autowired
    private ILinksService linksService;

    /**
     * 跳转到户外首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "links.html";
    }

    /**
     * 跳转到添加户外
     */
    @RequestMapping("/links_add")
    public String linksAdd() {
        return PREFIX + "links_add.html";
    }

    /**
     * 跳转到修改户外
     */
    @RequestMapping("/links_update/{linksId}")
    public String linksUpdate(@PathVariable Long linksId, Model model) {
        Links links = linksService.selectById(linksId);
        model.addAttribute("item",links);
        LogObjectHolder.me().set(links);
        return PREFIX + "links_edit.html";
    }

    /**
     * 获取户外列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper wrapper = new EntityWrapper();
        if(StrUtil.isNotBlank(condition))
            wrapper.like("title",condition);
        List<Map<String,Object>> list =  linksService.selectMaps(wrapper);
        return super.warpObject(new LinksWarpper(list));
    }

    /**
     * 新增户外
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Links links) {
        linksService.insert(links);
        return SUCCESS_TIP;
    }

    /**
     * 删除户外
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long linksId) {
        linksService.deleteById(linksId);
        return SUCCESS_TIP;
    }

    /**
     * 修改户外
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Links links) {
        linksService.updateById(links);
        return SUCCESS_TIP;
    }

    /**
     * 户外详情
     */
    @RequestMapping(value = "/detail/{linksId}")
    @ResponseBody
    public Object detail(@PathVariable("linksId") Long linksId) {
        return linksService.selectById(linksId);
    }
}
