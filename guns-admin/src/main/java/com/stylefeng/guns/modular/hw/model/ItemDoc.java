package com.stylefeng.guns.modular.hw.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 文章
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
@Data
@TableName("hw_item_doc")
public class ItemDoc extends Model<ItemDoc> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 栏目ID
     */
    private Long itemid;
    /**
     * 标题
     */
    private String title;
    /**
     * 图片
     */
    private String photo;
    /**
     * 内容
     */
    private String content;
    /**
     * 排序号
     */
    private Integer num;
    /**
     * 状态 :  0:禁用 1:启用
     */
    private Integer status;
    /**
     * 点击量
     */
    private Integer pv;
    /**
     * 访问量
     */
    private Integer uv;
    /**
     * 作者
     */
    private Long creater;
    /**
     * 创建时间
     */
    private Date createtime;
    /**
     * 更新时间
     */
    private Date updatetime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItemDoc{" +
        "id=" + id +
        ", itemid=" + itemid +
        ", title=" + title +
        ", photo=" + photo +
        ", content=" + content +
        ", num=" + num +
        ", status=" + status +
        ", pv=" + pv +
        ", uv=" + uv +
        ", creater=" + creater +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
