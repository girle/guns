package com.stylefeng.guns.modular.hw.dao;

import com.stylefeng.guns.modular.hw.model.Links;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 友情链接 Mapper 接口
 * </p>
 *
 * @author wangchuan
 * @since 2018-09-10
 */
public interface LinksMapper extends BaseMapper<Links> {

}
